package com.example.uiflow

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map

@ExperimentalCoroutinesApi
@FlowPreview
class FormViewModel : ViewModel() {

    private val userNameInput = ConflatedBroadcastChannel<String>("")
    val userLastNameInput = ConflatedBroadcastChannel<String>("")
    val submitFormInput = ConflatedBroadcastChannel<Unit>()


    val inputs = object : Inputs {
        override val userName = userNameInput
        override val userLastName = userLastNameInput
        override val submitForm = submitFormInput
    }

    val outputs = object : Outputs {
        val isSubmitButtonEnabledOutput =
            combine(userNameInput.asFlow(), userLastNameInput.asFlow()) { name, lastName ->
                Pair(
                    name,
                    lastName
                )
            }
                .map { it.first.isNotEmpty() && it.second.isNotEmpty() }
                .asLiveData()

        val submitFormResultOutput = submitFormInput.asFlow().asLiveData()

        override val isSubmitButtonEnabled = isSubmitButtonEnabledOutput
        override val submitFormResult = submitFormResultOutput
    }

    interface Inputs {
        val userName: SendChannel<String>
        val userLastName: SendChannel<String>
        val submitForm: SendChannel<Unit>
    }

    interface Outputs {
        val isSubmitButtonEnabled: LiveData<Boolean>
        val submitFormResult: LiveData<Unit>
    }
}