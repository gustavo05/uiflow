package com.example.uiflow

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: FormViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProvider(this, FormViewModelFactory()).get(FormViewModel::class.java)
        initUi()
        initOutputs()
    }

    private fun initUi() {
        with(viewModel.inputs) {
            edtName.doOnTextChanged { text, _, _, _ -> userName.offer(text.toString()) }
            edtLastName.doOnTextChanged { text, _, _, _ -> userLastName.offer(text.toString()) }
            btnSubmit.setOnClickListener { submitForm.offer(Unit) }
        }
    }

    private fun initOutputs() {
        with(viewModel.outputs) {
            isSubmitButtonEnabled.observe(this@MainActivity, Observer { setButtonEnabled(it) })
            submitFormResult.observe(this@MainActivity, Observer { userSaved() })
        }
    }

    private fun setButtonEnabled(isEnabled: Boolean) {
        btnSubmit.isEnabled = isEnabled
    }

    private fun userSaved() {
        Toast.makeText(this@MainActivity, "User saved!!", Toast.LENGTH_LONG).show()
        edtName.setText("")
        edtLastName.setText("")
    }
}

@ExperimentalCoroutinesApi
@FlowPreview
class FormViewModelFactory : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FormViewModel() as T
    }
}